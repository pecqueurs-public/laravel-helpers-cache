<?php

namespace PecqueurS\LaravelHelpers\Cache;

use PecqueurS\LaravelHelpers\Cache\Traits\StoreTrait;

abstract class CacheBase
{
    use StoreTrait;
    
    public static function make(...$arguments): static
    {
        return new static(...$arguments); 
    }
    
    abstract public function getKey(): string;
}