<?php

namespace PecqueurS\LaravelHelpers\Cache;

use PecqueurS\LaravelHelpers\Cache\Traits\ForgetTrait;
use PecqueurS\LaravelHelpers\Cache\Traits\GenerateTrait;
use PecqueurS\LaravelHelpers\Cache\Traits\GetterTrait;
use PecqueurS\LaravelHelpers\Cache\Traits\HasTrait;

abstract class CachePuller extends CacheBase
{
    use HasTrait, GetterTrait, ForgetTrait, GenerateTrait;
    
    protected ?int $ttl = 60; // Time to live
    
    public function init(): void
    {
        $this->getCache()->put($this->getKey(), $this->generate(), $this->ttl);
    }

    public function pull(): mixed
    {
        return $this->getCache()->pull($this->getKey());
    }
}
