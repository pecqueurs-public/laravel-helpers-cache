<?php

namespace PecqueurS\LaravelHelpers\Cache;

use Illuminate\Contracts\Cache\Lock;

abstract class CacheLocker extends CacheBase
{
    protected ?int $lockTime = 10; // Time to lock

    public function lock(): Lock
    {
        return $this->getCache()->lock($this->getKey(), $this->lockTime);
    }

    public function get(?callable $callback = null): mixed
    {
        $this->lock()->get($callback);
    }

    public function block(int $seconds, ?callable $callback = null)
    {
        $this->lock()->block($seconds, $callback);
    }
    
    public function release(): void 
    {
        $this->getCache()->lock($this->getKey())->forceRelease();
    }
}
