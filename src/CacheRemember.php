<?php

namespace PecqueurS\LaravelHelpers\Cache;

use PecqueurS\LaravelHelpers\Cache\Traits\ForgetTrait;
use PecqueurS\LaravelHelpers\Cache\Traits\GenerateTrait;
use PecqueurS\LaravelHelpers\Cache\Traits\GetterTrait;
use PecqueurS\LaravelHelpers\Cache\Traits\HasTrait;

abstract class CacheRemember extends CacheBase
{
    use HasTrait, GetterTrait, ForgetTrait, GenerateTrait;
    
    protected ?int $ttl = 60; // Time to live
    protected bool $setOnCache = true; // Use cache

    public function value(): mixed 
    {
        return !$this->setOnCache 
            ? $this->generate() 
            : (
                $this->ttl 
                ? $this->getCache()->remember(
                    $this->getKey(),
                    $this->ttl,
                    fn () => $this->generate()
                )
                : $this->getCache()->rememberForever(
                    $this->getKey(),
                    fn () => $this->generate()
                )
            );
    }
}
