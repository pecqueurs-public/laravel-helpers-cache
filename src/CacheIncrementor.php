<?php

namespace PecqueurS\LaravelHelpers\Cache;

use PecqueurS\LaravelHelpers\Cache\Traits\ForgetTrait;
use PecqueurS\LaravelHelpers\Cache\Traits\GetterTrait;
use PecqueurS\LaravelHelpers\Cache\Traits\HasTrait;

abstract class CacheIncrementor extends CacheBase
{
    use HasTrait, GetterTrait, ForgetTrait;
    
    protected int $initialValue = 0;
    protected ?int $step = 1;
    protected ?int $ttl = null; // Time to live

    protected function initValue(): void
    {
        $this->getCache()->add($this->getKey(), $this->initialValue, $this->ttl);
    }

    public function increment(?int $value = null): int
    {
        $this->initValue();

        return $this->getCache()->increment($this->getKey(), $value ?? $this->step ?? 1);
    }

    public function decrement(?int $value = null): int
    {
        $this->initValue();

        return $this->getCache()->decrement($this->getKey(), $value ?? $this->step ?? 1);
    }
}
