<?php

namespace PecqueurS\LaravelHelpers\Cache\Traits;

trait HasTrait
{
    public function has(): bool 
    {
        return $this->getCache()->has($this->getKey());
    }
}
