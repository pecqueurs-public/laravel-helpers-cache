<?php

namespace PecqueurS\LaravelHelpers\Cache\Traits;

trait ForgetTrait
{
    public function forget(): bool 
    {
        return $this->getCache()->forget($this->getKey());
    }
}
