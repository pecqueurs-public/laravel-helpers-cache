<?php

namespace PecqueurS\LaravelHelpers\Cache\Traits;

trait GenerateTrait
{
    abstract protected function generate(): mixed; 
}