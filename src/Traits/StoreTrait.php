<?php

namespace PecqueurS\LaravelHelpers\Cache\Traits;

use Illuminate\Support\Facades\Cache;
use Illuminate\Cache\Repository;

trait StoreTrait
{
    protected ?string $store = null; // driver to use for storing the cache
    
    protected function getStore(): string
    {
        return $this->store ?? config('cache.default');
    }

    protected function getCache(): Repository
    {
        return Cache::store($this->getStore());
    }
}
