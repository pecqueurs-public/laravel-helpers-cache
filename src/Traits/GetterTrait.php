<?php

namespace PecqueurS\LaravelHelpers\Cache\Traits;

trait GetterTrait
{
    public function get(?callable $callback = null, bool $generate = false): mixed
    {
        return $callback
            ? $this->getCache()->get($this->getKey(), fn () => $callback())
            : (method_exists($this, 'generate') && $generate
                ? $this->getCache()->get($this->getKey(), fn () => $this->generate())
                : $this->getCache()->get($this->getKey())
            );
    }
}
