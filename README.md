# Laravel Helpers CACHE

## objective

helper to facilitate cache management.

## Get started

### Install

```bash
composer config repositories.gitlab.com/pecqueurs-public composer https://gitlab.com/api/v4/group/53853709/-/packages/composer/

composer require pecqueurs/laravel-helpers-cache
```

### Usage

Create file named `Cache/MyCacheElement.php`.

#### Remember type

Retrieve and Store 

##### Definition
```php
class MyCacheElement extends CacheRemember
{
    protected ?int $ttl = 60; 
    
    public function __construct(
        protected readonly User $user,
        bool $setOnCache = true
    ) {
        $this->setOnCache = $setOnCache;
    }

    public function getKey(): string
    {
        return "user:{$this->user->id}";
    }

    protected function generate(): mixed
    {
        return $this->user->toArray();
    }
}
```

##### Usage
```php
// Remember
MyCacheElement::make(User::first())->value();

// Has value on cache
MyCacheElement::make(User::first())->has();

// Get value if exist (don't set if not)
MyCacheElement::make(User::first())->get();

// Forget value
MyCacheElement::make(User::first())->forget();
```


#### Incrementor type

Incrementing / Decrementing Values

##### Definition
```php
class MyCacheElement extends CacheIncrementor
{
    protected int $initialValue = 0;
    protected ?int $step = 1;
    protected ?int $ttl = null;
    
    public function __construct(
        protected readonly User $user,
    ) {}

    public function getKey(): string
    {
        return "user-hit:{$this->user->id}";
    }
}
```

##### Usage
```php
// increment default value
MyCacheElement::make(User::first())->increment();

// increment specific value
MyCacheElement::make(User::first())->increment(2);

// decrement default value
MyCacheElement::make(User::first())->decrement();

// decrement specific value
MyCacheElement::make(User::first())->decrement(2);

// Has value on cache
MyCacheElement::make(User::first())->has();

// Get value if exist (don't set if not)
MyCacheElement::make(User::first())->get();

// Forget value
MyCacheElement::make(User::first())->forget();
```


#### Puller type

Retrieve and Delete

##### Definition
```php
class MyCacheElement extends CachePuller
{
    protected ?int $ttl = null;
    
    public function __construct(
        protected readonly User $user,
    ) {}

    public function getKey(): string
    {
        return "user-consume-token:{$this->user->id}";
    }

    protected function generate(): mixed
    {
        return Str::random();
    }
}
```

##### Usage
```php
// Init
MyCacheElement::make(User::first())->init();

// Pull
MyCacheElement::make(User::first())->pull();

// Has value on cache
MyCacheElement::make(User::first())->has();

// Get value if exist (don't set if not)
MyCacheElement::make(User::first())->get();

// Forget value
MyCacheElement::make(User::first())->forget();
```


#### Locker type

Atomic Locks

##### Definition
```php
class MyCacheElement extends CacheLocker
{
    protected ?int $lockTime = 10;

    public function __construct(
        protected readonly User $user,
    ) {}

    public function getKey(): string
    {
        return "user-lock-action:{$this->user->id}";
    }
}
```

##### Usage
```php
// Return Lock instance
MyCacheElement::make(User::first())->lock();

// Get
MyCacheElement::make(User::first())->get();
MyCacheElement::make(User::first())->get(function() {
    // Lock acquired for 10 seconds and automatically released...
});

// Block
MyCacheElement::make(User::first())->block(5);
MyCacheElement::make(User::first())->block(5, function () {
    // Lock acquired after waiting a maximum of 5 seconds...
});

// Release
MyCacheElement::make(User::first())->release();
```


#### Set other Cache Store
```php
class MyCacheElement extends CacheRemember
{
    protected ?int $store = 'redis'; 
}
```


#### Getter
```php
// Get value if exist (don't create if not)
MyCacheElement::make(User::first())->get();


// Get value if exist (create if not without set in cache)
MyCacheElement::make(User::first())->get(generate: true);


// Get value if exist (return default if not without set in cache)
MyCacheElement::make(User::first())->get(fn() => 'default value');
```
